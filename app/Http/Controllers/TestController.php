<?php
/*
 * @Author: SanQian
 * @Date: 2021-08-10 09:24:48
 * @LastEditTime: 2021-10-25 17:31:16
 * @LastEditors: SanQian
 * @Description:
 * @FilePath: /huoban_basic_backend/app/Http/Controllers/TestController.php
 *
 */

namespace App\Http\Controllers;

use App\ModelsHuoban\HuobanCreate;
use App\ModelsHuoban\HuobanExample;

class TestController extends Controller
{
    public function test()
    {
        $_huoban = HuobanCreate::create(config('huoban.huoban_pass'));

        new HuobanExample($_huoban);
        $response = $_huoban->_members->getMembers(4000000002891045);

        print_r($response);die();
    }
}
