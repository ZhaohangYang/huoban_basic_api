<?php
/*
 * @Author: SanQian
 * @Date: 2021-08-10 10:15:47
 * @LastEditTime: 2021-09-15 14:21:56
 * @LastEditors: SanQian
 * @Description:
 * @FilePath: /huoban_basic/huoban_basic_backend/config/huoban.php
 *
 */

return [
    'huoban_pass' => [
        // 应用信息
        'application_id'     => env('HUOBAN_APP_ID'),
        'application_secret' => env('HUOBAN_APP_SECRET'),

        // 配置名称
        'name'               => 'huoban_pass',
        // 是否启用别名模式
        'alias_model'        => true,
        // 权限类别  enterprise/table
        'app_type'           => 'enterprise',
        // 工作区id
        'space_id'           => '',
        // api地址
        'api_url'            => 'https://api.huoban.com',
        // 上传文件地址
        'upload_url'         => 'https://upload.huoban.com',
        // 缓存文件存放地址
        'cache_path'         => storage_path() . '/huoban/',
    ],

];
